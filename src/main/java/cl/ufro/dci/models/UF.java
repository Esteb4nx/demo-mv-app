package cl.ufro.dci.models;

import java.util.Date;

public class UF {
    private Date fecha;
    private double valor;

    public UF(Date fecha, double valor) {
        this.fecha = fecha;
        this.valor = valor;
    }

    public UF() {
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
