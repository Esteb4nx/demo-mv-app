package cl.ufro.dci.utils;

import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Utils {

    public Double extractUF() throws IOException {
        Document doc = Jsoup.connect("https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=").get();
        Elements newsHeadlines = doc.select("#gr_ctl07_Mayo");
        for (Element headline : newsHeadlines) {
            log("%s\n", headline.text());
            return Double.valueOf( headline.text());
        }

        return null;
    }

    private static void log(String msg, String... vals) {
        System.out.printf((msg) + "%n", vals);
    }
}
